# ABCD Compiler

University of Melbourne, COMP30020 Declarative Programming, Project 2  
By [Aram Kocharyan](http://aramk.com/), 2012

Please see `spec.html`.

## Description

It is assumed that abcdrt.m is also present in the same directory. The output .m
file is placed in the same directory also.

abcdc supports an -e argument to specify the entry function much like in proj1:

    Usage: abcdc [-efunc] [-p] [-u] Name

It will check that "func" is a valid entry point, and the default is "main". If
the entry point doesn't exist, it will print a list of valid entry points:

    $ ./abcdc -etest ../abcd_progs/treesort < ../abcd_progs/in3
    abcdc: No entry 'test' found!
    Try: main,bst_insert,bst_traverse,append

The -p argument will print out the compiled ABCD program.
The -u argument turns on the unboxing optimisation.
The name of the ABCD file can either contain or omit the extension.
It supports reading input using redirection (<) as in proj1.

## Optimisations & Features

While predicates only contain the variables they need, according to this rule:

>	An output Var is also an input Var, but an input Var is not necessarily
>    output Var.
    
An output Var is a left hand side assignment, and an input Var is a right hand
side reference.

Simplification of ITE and While guards is also implemented, and actually uses
the same code applied to different contexts.

As mentioned before, function entries and initial variables can be specified.

Nested while loops are supported.

Unboxing has been almost fully implemented for while loops and can be activated
using the "-u" argument. I initially planned to have unboxing throughout the
entire program, then realised the troubles it would cause when passing variables,
needing to remember which variable was a lum and which was an int and so forth.
The following are complete for while loops:

 * The Context object that is passed around stores an IntVars object containing the variables which are int input arguments and the variables which are currently ints (depending on the current position in the while loop). It also contains the intMode which specifies to lower functions that unboxing should occur if possible. At the moment this mode is only enabled for While loops and only if Unboxing is turned on with "-u".
 
 * The compileTerm function uses the mode and passes around a Bool called the intTerm which is true if the entire Term will evaluate to an int. For now this is used only for integer arithmetic (like Plus). This is used to acknowledge to a unification that the variable assigned to is now also an int.
 
 * The predicate declaration is successfully implemented to show which inputs are int and which are lum.
 
 * If an int variable is in a while loop with unboxing enabled and is in a term that does NOT evaluate to an integer, it will box itself up again.
 
 * The predicate call to the while is not implemented yet
 
So to summarise, the unboxing is not ready for running tests on, but is almost
complete. All other optimisations are done however.

# Methodology

The compiler is separated into two parts:

1. The "translate" functions which turn an ABCD Prog into a Mercury MProg
2. The "compile" functions which turn an MProg into Mercury code

In order to ensure maintainable data structures, an MProg is void of any state
data such as the numbering of ABCD variables in the Mercury code. If these were
calculated and included at translate time, then an MProg would be a better
storage for ABCD specific Mercury programs than a compiled string, but would
suffer similar issues of unmaintainable structure. If for example, I wanted to
insert a new Unif ("unification") type into an MFunc, I would need to take into
account not only the variable enumeration at the insertion point, but also
update all enumerations following my insertion. For example, if I wanted to add
a new A Var, I would find that A4 was the A at that position, insert my A5 and
increment the numbering of all subsequent A variables. In effect the MProg would
be storing redundant information with convenience as a merit. The argument is
similar for optimisations - these are performed on any input MProg in the
compile functions and not in the output MProg of the translate functions.

## Modules

- Compiler.hs contains the translate and compile functions, along with compiler
specific data structures.

- Main.hs has been altered to allow function entries, standard input and other
features mentioned in the Description.

- MProg.hs contains ABCD specific Mercury data structures, as well as definitions
of common strings used in abcdrt.m. Note that as the spec states, this isn't a
complete representation of Mercury. For instance, although a While is really a
predicate, they would not be used for any other purpose so for simplicity I have
omitted them and treated the WhileM as a Goal - which can appear in a
conjunction and is compiled into a predicate call and a while predicate.

- Prog.hs contains several new data types used in the compiler but not specific to
its functionality - as a result I've placed them among the other ABCD specific
data structures. Along with these are utility functions that work on these data
structures.

- Parse.hs has not been altered.

- Util.hs contains a set of generic Haskell functions used by the compiler but can
be applied in any program.

- abcdrt.m has several new functions to operate on unboxed variables.
