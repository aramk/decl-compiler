% Compiled ABCD program
:- module reverse. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_reverse(abcdrt_num(0),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_reverse(lum,lum,lum,lum) = lum.
abcd_reverse(A1,B1,C1,D1) = A4 :- B2 = abcdrt_nil,abcdw_0(A1,B2,C1,D1,A3,B4,C2,D2),A4 = B4.

:- pred abcdw_0(lum::in,lum::in,lum::in,lum::in,lum::out,lum::out,lum::out,lum::out) is det.
abcdw_0(A1,B2,C1,D1,A0,B0,C0,D0) :-
(abcdrt_not(abcdrt_equal(A1,abcdrt_nil)) = abcdrt_num(1) -> B3 = abcdrt_cons(abcdrt_head(A1),B2),A2 = abcdrt_tail(A1),abcdw_0(A2,B3,C1,D1,A0,B0,C0,D0) ; A0 = A1,B0 = B2,C0 = C1,D0 = D1).
