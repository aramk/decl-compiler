% Compiled ABCD program
:- module ../abcd_progs/sum_tr. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_sum_tr(abcdrt_num(0),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_sum_tr(lum,lum,lum,lum) = lum.
abcd_sum_tr(A1,B1,C1,D1) = A2 :- C2 = abcdrt_num(0),B2 = A1,A2 = abcd_sum_acc(A1,B2,C2,D1).
:- func abcd_sum_acc(lum,lum,lum,lum) = lum.
abcd_sum_acc(A1,B1,C1,D1) = A2 :- (abcdrt_not(abcdrt_equal(B1,abcdrt_nil)) = abcdrt_num(1) -> C2 = abcdrt_plus(C1,abcdrt_head(B1)),B2 = abcdrt_tail(B1),A2 = abcd_sum_acc(A1,B2,C2,D1) ; A2 = C1,B2 = B1,C2 = C1).
