% Compiled ABCD program
:- module append. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_append(abcdrt_cons(abcdrt_num(1),abcdrt_cons(abcdrt_num(2),abcdrt_nil)),abcdrt_cons(abcdrt_num(3),abcdrt_cons(abcdrt_num(4),abcdrt_nil)),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_append(lum,lum,lum,lum) = lum.
abcd_append(A1,B1,C1,D1) = A7 :- D2 = abcdrt_nil,abcdw_0(A1,D2,A3,D4),A4 = D4,abcdw_1(A4,B1,D4,A6,B3,D5),A7 = B3.

:- pred abcdw_0(lum::in,lum::in,lum::out,lum::out) is det.
abcdw_0(A1,D2,A0,D0) :-
(abcdrt_not(abcdrt_equal(A1,abcdrt_nil)) = abcdrt_num(1) -> D3 = abcdrt_cons(abcdrt_head(A1),D2),A2 = abcdrt_tail(A1),abcdw_0(A2,D3,A0,D0) ; A0 = A1,D0 = D2).

:- pred abcdw_1(lum::in,lum::in,lum::in,lum::out,lum::out,lum::out) is det.
abcdw_1(A4,B1,D4,A0,B0,D0) :-
(abcdrt_not(abcdrt_equal(A4,abcdrt_nil)) = abcdrt_num(1) -> B2 = abcdrt_cons(abcdrt_head(A4),B1),A5 = abcdrt_tail(A4),abcdw_1(A5,B2,D4,A0,B0,D0) ; A0 = A4,B0 = B1,D0 = D4).
