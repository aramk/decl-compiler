% Compiled ABCD program
:- module app_r2. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_main(abcdrt_num(0),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_main(lum,lum,lum,lum) = lum.
abcd_main(A1,B1,C1,D1) = A2 :-
	(abcdrt_not(abcdrt_equal(B1,abcdrt_nil)) = abcdrt_num(1) ->
		D2 = abcdrt_head(B1),
		B2 = abcdrt_tail(B1),
		A2 = A1
	;
		A2 = C1,
		B2 = B1,
		D2 = D1).
