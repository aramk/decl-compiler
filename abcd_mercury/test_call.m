% Compiled ABCD program
:- module test_call. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_main(abcdrt_num(0),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_main(lum,lum,lum,lum) = lum.
abcd_main(A1,B1,C1,D1) = A5 :- (abcdrt_equal(B1,abcdrt_nil) = abcdrt_num(1) -> A2 = abcdrt_head(B1),A3 = abcdrt_num(5),A4 = abcdrt_num(7),A5 = abcdrt_num(8),B2 = abcdrt_num(6) ; A2 = C1,A3 = abcdrt_plus(A2,abcdrt_num(5)),A5 = A3,B2 = B1).
