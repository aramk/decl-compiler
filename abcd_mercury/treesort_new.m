% Compiled ABCD program
:- module treesort. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_main(abcdrt_cons(abcdrt_num(4),abcdrt_cons(abcdrt_num(6),abcdrt_cons(abcdrt_num(5),abcdrt_cons(abcdrt_num(1),abcdrt_cons(abcdrt_num(3),abcdrt_cons(abcdrt_num(2),abcdrt_nil)))))),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_main(lum,lum,lum,lum) = lum.
abcd_main(A1,B1,C1,D1) = A5 :- C2 = A1,A2 = abcdrt_nil,abcdw_0(A2,B1,C2,D1,A4,B3,C4),A5 = abcd_bst_traverse(A4,B3,C4,D1).

:- func abcd_bst_insert(lum,lum,lum,lum) = lum.
abcd_bst_insert(A1,B1,C1,D1) = A3 :- (A1 = abcdrt_nil -> A2 = abcdrt_cons(B1,abcdrt_cons(abcdrt_nil,abcdrt_nil)),A3 = A2,C2 = C1,D2 = D1 ; C2 = abcdrt_head(A1),D2 = abcdrt_tail(A1),(abcdrt_less(B1,C2) = abcdrt_num(1) -> A2 = abcdrt_head(D2),A3 = abcdrt_cons(C2,abcdrt_cons(abcd_bst_insert(A2,B1,C2,D2),abcdrt_tail(D2))) ; A2 = abcdrt_tail(D2),A3 = abcdrt_cons(C2,abcdrt_cons(abcdrt_head(D2),abcd_bst_insert(A2,B1,C2,D2))))).

:- func abcd_bst_traverse(lum,lum,lum,lum) = lum.
abcd_bst_traverse(A1,B1,C1,D1) = A6 :- (A1 = abcdrt_nil -> A6 = A1,B3 = B1,C2 = C1 ; C2 = A1,A2 = abcdrt_tail(abcdrt_tail(C2)),B2 = abcd_bst_traverse(A2,B1,C2,D1),A3 = abcdrt_cons(abcdrt_head(C2),abcdrt_nil),B3 = abcd_append(A3,B2,C2,D1),A4 = abcdrt_head(abcdrt_tail(C2)),A5 = abcd_bst_traverse(A4,B3,C2,D1),A6 = abcd_append(A5,B3,C2,D1)).

:- func abcd_append(lum,lum,lum,lum) = lum.
abcd_append(A1,B1,C1,D1) = A3 :- (A1 = abcdrt_nil -> A2 = B1,A3 = A2,C2 = C1 ; C2 = abcdrt_head(A1),A2 = abcdrt_tail(A1),A3 = abcdrt_cons(C2,abcd_append(A2,B1,C2,D1))).

:- pred abcdw_0(lum::in,lum::in,lum::in,lum::in,lum::out,lum::out,lum::out) is det.
abcdw_0(A2,B1,C2,D1,A0,B0,C0) :-
(C2 = abcdrt_nil -> A0 = A2,B0 = B1,C0 = C2 ; B2 = abcdrt_head(C2),A3 = abcd_bst_insert(A2,B2,C2,D1),C3 = abcdrt_tail(C2),abcdw_0(A3,B2,C3,D1,A0,B0,C0)).
