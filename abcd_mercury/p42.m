% Compiled ABCD program
:- module p42. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_main(abcdrt_num(0),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_main(lum,lum,lum,lum) = lum.
abcd_main(A1,B1,C1,D1) = A2 :- A2 = abcdrt_num(42).
