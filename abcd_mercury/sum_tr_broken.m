% Compiled ABCD program
:- module sum_tr. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_sum_tr(abcdrt_num(0),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_sum_tr(lum,lum,lum,lum) = lum.
abcd_sum_tr(A1,B1,C1,D1) = A3 :-
	%% CANNOT - before each func body, check which vars are ints - BUT THEY CAN CHANGE DURING BODY AFTER REASSIGN!!!
	
	A2 = abcdrt_cons(abcdrt_num(1),abcdrt_cons(abcdrt_num(2),abcdrt_nil)),    % not an int!
	C2 = 0,%abcdrt_num(0), % now an int   %% still an int!
	B2 = A2,     % not an int!
	A3 = abcd_sum_acc(A2,B2,C2,D1).       % D1 not sure

:- func abcd_sum_acc(lum,lum,int,lum) = lum.  % arg change
abcd_sum_acc(A1,B1,C1,D1) = A2 :- (B1 = abcdrt_nil ->
	A2 = C1,B2 = B1,C2 = C1
;
	C2 = abcdrt_plusi(C1,abcdrt_headi(B1)),   % func change
	B2 = abcdrt_tail(B1),
	A2 = abcd_sum_acc(A1,B2,C2,D1)		% func is recursive (so change its args, any other func call must box back up)
).
