% Compiled ABCD program
:- module sumg_r. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_main(abcdrt_num(0),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_sum_r(lum,lum,lum,lum) = lum.
abcd_sum_r(A1,B1,C1,D1) = A3 :- (abcdrt_equal(B1,abcdrt_nil) = abcdrt_num(1) -> A2 = abcdrt_num(0),A3 = A2,B3 = B1,C2 = C1 ; (abcdrt_isnum(B1) = abcdrt_num(1) -> A2 = B1,A3 = A2,B3 = B1,C2 = C1 ; C2 = abcdrt_tail(B1),B2 = abcdrt_head(B1),A2 = abcd_sum_r(A1,B2,C2,D1),B3 = C2,A3 = abcdrt_plus(A2,abcd_sum_r(A2,B3,C2,D1)))).
:- func abcd_main(lum,lum,lum,lum) = lum.
abcd_main(A1,B1,C1,D1) = A2 :- B2 = abcdrt_cons(abcdrt_nil,abcdrt_cons(abcdrt_cons(abcdrt_num(1),abcdrt_num(2)),abcdrt_cons(abcdrt_num(3),abcdrt_nil))),A2 = abcd_sum_r(A1,B2,C1,D1).
