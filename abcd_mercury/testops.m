% Compiled ABCD program
:- module testops. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_main(abcdrt_num(0),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_main(lum,lum,lum,lum) = lum.
abcd_main(A1,B1,C1,D1) = A2 :- A2 = abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_cons(abcdrt_times(abcdrt_num(4),abcdrt_num(2)),abcdrt_div(abcdrt_num(8),abcdrt_num(2))),abcdrt_plus(abcdrt_num(1),abcdrt_num(1))),abcdrt_minus(abcdrt_num(4),abcdrt_num(3))),abcdrt_less(abcdrt_num(4),abcdrt_num(2))),abcdrt_equal(abcdrt_num(2),abcdrt_num(2))),abcdrt_greater(abcdrt_num(1),abcdrt_num(2))),abcdrt_not(abcdrt_num(0))),abcdrt_and(abcdrt_num(1),abcdrt_num(0))),abcdrt_or(abcdrt_num(1),abcdrt_num(0))),abcdrt_head(abcdrt_cons(abcdrt_num(0),abcdrt_num(2)))),abcdrt_tail(abcdrt_cons(abcdrt_num(4),abcdrt_num(1)))),abcdrt_isnum(abcdrt_cons(abcdrt_num(4),abcdrt_num(2)))),abcdrt_nil).
