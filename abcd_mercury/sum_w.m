% Compiled ABCD program
:- module sum_w. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_sum_w(abcdrt_cons(abcdrt_num(10),abcdrt_cons(abcdrt_num(45),abcdrt_nil)),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_sum_w(lum,lum,lum,lum) = lum.
abcd_sum_w(A1,B1,C1,D1) = A4 :-
	C2 = abcdrt_num(0),   % this can't be assumed int - what if used in other func calls?
	abcdw_0(A1,abcdrt_int(C2),A3,C4),    % int inputs are unboxed! outputs untouched here - must do after:
	C5 = abcdrt_num(C4),
	
	% this way is dangerous, since C2 is int and might need boxing again and again each time for other func calls!!! maybe just turn the return of all ints from the while into lums again.
	
	A4 = C5.    % notice that a is also now an int!

:- pred abcdw_0(lum::in,int::in,lum::out,int::out) is det.
abcdw_0(A1,C2,A0,C0) :-
(A1 = abcdrt_nil -> A0 = A1,C0 = C2
	;
C3 = abcdrt_plusi(D1,abcdrt_headi(A1)),  % C3 is int, so is C2. C is int. If C2 as D1, then D is also int.
A2 = abcdrt_tail(A1),
% if call to another func, box them and unbox if A is int here...
abcdw_0(A2,C3,A0,C0)
).
