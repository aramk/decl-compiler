% Compiled ABCD program
:- module sum_r. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_main(abcdrt_num(0),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_sum_r(lum,lum,lum,lum) = lum.
abcd_sum_r(A1,B1,C1,D1) = A2 :- (abcdrt_not(abcdrt_equal(B1,abcdrt_nil)) = abcdrt_num(1) -> C2 = abcdrt_head(B1),B2 = abcdrt_tail(B1),A2 = abcdrt_plus(C2,abcd_sum_r(A1,B2,C2,D1)) ; A2 = abcdrt_num(0),B2 = B1,C2 = C1).

:- func abcd_main(lum,lum,lum,lum) = lum.
abcd_main(A1,B1,C1,D1) = A2 :- B2 = abcdrt_cons(abcdrt_num(1),abcdrt_cons(abcdrt_num(2),abcdrt_cons(abcdrt_num(3),abcdrt_cons(abcdrt_num(4),abcdrt_nil)))),A2 = abcd_sum_r(A1,B2,C1,D1).
