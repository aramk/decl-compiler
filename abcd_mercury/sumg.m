% Compiled ABCD program
:- module sumg. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_main(abcdrt_num(0),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_sum(lum,lum,lum,lum) = lum.
abcd_sum(A1,B1,C1,D1) = A4 :- A2 = abcdrt_num(0),abcdw_0(A2,B1,C1,D1,A4,B4,C3,D2).
:- func abcd_main(lum,lum,lum,lum) = lum.
abcd_main(A1,B1,C1,D1) = A2 :- B2 = abcdrt_cons(abcdrt_nil,abcdrt_cons(abcdrt_cons(abcdrt_num(1),abcdrt_num(2)),abcdrt_cons(abcdrt_num(3),abcdrt_nil))),A2 = abcd_sum(A1,B2,C1,D1).

:- pred abcdw_0(lum::in,lum::in,lum::in,lum::in,lum::out,lum::out,lum::out,lum::out) is det.
abcdw_0(A2,B1,C1,D1,A0,B0,C0,D0) :-
(abcdrt_not(abcdrt_equal(B1,abcdrt_nil)) = abcdrt_num(1) -> (abcdrt_isnum(B1) = abcdrt_num(1) -> A3 = abcdrt_plus(A2,B1),B2 = abcdrt_nil,B3 = B2,C2 = C1 ; C2 = abcdrt_tail(B1),B2 = abcdrt_head(B1),(abcdrt_isnum(B2) = abcdrt_num(1) -> A3 = abcdrt_plus(A2,B2),B3 = C2 ; (abcdrt_equal(B2,abcdrt_nil) = abcdrt_num(1) -> B3 = C2 ; B3 = abcdrt_cons(abcdrt_head(B2),abcdrt_cons(abcdrt_tail(B2),C2))),A3 = A2)),abcdw_0(A3,B3,C2,D1,A0,B0,C0,D0) ; A0 = A2,B0 = B1,C0 = C1,D0 = D1).
