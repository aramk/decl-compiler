% Compiled ABCD program
:- module sum_w. :- interface.
:- pred main(io.state::di, io.state::uo) is det.
:- import_module io.
:- implementation.
:- import_module abcdrt.
main --> abcdrt_write_lum(abcd_sum_w(abcdrt_cons(abcdrt_num(10),abcdrt_cons(abcdrt_num(45),abcdrt_nil)),abcdrt_num(0),abcdrt_num(0),abcdrt_num(0))), nl.

:- func abcd_sum_w(lum,lum,lum,lum) = lum.
abcd_sum_w(A1,B1,C1,D1) = abcdrt_num(A4) :- % if A is int then box
	C2 = 0,
	abcdw_0(A1,C2,A3,C4),
	A4 = C4.    % notice that a is also now an int!

:- pred abcdw_0(lum::in,int::in,lum::out,int::out) is det.
abcdw_0(A1,C2,A0,C0) :-
(A1 = abcdrt_nil -> A0 = A1,C0 = C2
	;
C3 = abcdrt_plusi(C2,abcdrt_headi(A1)),
A2 = abcdrt_tail(A1),
abcdw_0(A2,C3,A0,C0)
).
