-------------------------------------------------------------------------------
--
-- vim: sw=4 ts=4 et ft=haskell
--
-- An ABCD specific representation of the Mercury language.
--
-- HIDE_START
-- Written by Aram Kocharyan, aramk
-- HIDE_END
--

-- TODO add
module MProg (
		funcNamePrefix,
		runTimePrefix,
		whilePrefix,
		lumName,
		Term(..),
		Goal(..),
		Rule(..),
		-- PredMode(..),
		-- Procedure(..),
		FuncM(..),
		-- WhileM(..),
		MProg(..)
	) where

import Prog

funcNamePrefix :: String
funcNamePrefix = "abcd_"

runTimePrefix :: String
runTimePrefix = "abcdrt_"

whilePrefix :: String
whilePrefix = "abcdw_"

lumName :: String
lumName = "lum"

data Term
	= VarM Var
	| NumM Int
	
	-- ABCD runtime function calls
	| NilM
    | ConsM Term Term
    | PlusM Term Term
    | MinusM Term Term
    | TimesM Term Term
    | DivM Term Term
    | EqualM Term Term -- possible?
    | LessM Term Term
    | GreaterM Term Term
    | NotM Term
    | IsnumM Term
    | AndM Term Term
    | OrM Term Term
    | HeadM Term
    | TailM Term

	-- ABCD internal function call
    | CallM String
	deriving (Show)

-- http://www.mercury.csse.unimelb.edu.au/information/doc-release/mercury_ref/Goals.html#Goals
data Goal
	= Unif Var Term
	| Conj [Goal]
	| Disj [Goal]
	| ITEM Term Goal Goal
	| WhileM Rule
    deriving (Show)

-- For Clauses, Facts are not needed
data Rule = Rule Term Goal
    deriving (Show)

data FuncM = FuncM String Rule deriving (Show)

data MProg = MProg [FuncM]
    deriving (Show)
