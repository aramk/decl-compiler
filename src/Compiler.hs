-------------------------------------------------------------------------------
--
-- vim: sw=4 ts=4 et ft=haskell
--
-- A compiler for the ABCD language into Mercury.
--
-- HIDE_START
-- Written by Aram Kocharyan, aramk
-- HIDE_END
--

module Compiler(translate, compile, funcRuntimeSig, MaybeUnbox(..)) where

import Util
import Prog
import MProg
import Data.List

-- TRANSLATION =================================================================

-- Translates ABCD Prog into Mercury MProg
translate :: Prog -> MProg
translate (Prog funcs) = MProg (translateFuncs funcs)

translateFuncs :: [Func] -> [FuncM]
translateFuncs fs = (map translateFunc fs)

translateFunc :: Func -> FuncM
translateFunc (Func name stmts) =
	FuncM name (Rule (VarM A) (translateStmts stmts))

translateStmts :: [Stmt] -> Goal
translateStmts xs = Conj (map translateStmt xs)

translateStmt :: Stmt -> Goal
translateStmt stmt = case stmt of
	(Assign v e) -> Unif v (translateExpr e)
	(ITE e stmts1 stmts2) ->
		ITEM (translateExpr e) (translateStmts stmts1) (translateStmts stmts2)
	(While e stmts) -> WhileM (Rule (translateExpr e) (translateStmts stmts))

translateExpr :: Expr -> Term
translateExpr e = case e of
	Var v -> VarM v
	NumE n -> NumM n
	
	NilE -> NilM
	ConsE e1 e2 -> translateExpr2 ConsM e1 e2
	Plus e1 e2 -> translateExpr2 PlusM e1 e2
	Minus e1 e2 -> translateExpr2 MinusM e1 e2
	Times e1 e2 -> translateExpr2 TimesM e1 e2
	Div e1 e2 -> translateExpr2 DivM e1 e2
	Equal e1 e2 -> translateExpr2 EqualM e1 e2
	Less e1 e2 -> translateExpr2 LessM e1 e2
	Greater e1 e2 -> translateExpr2 GreaterM e1 e2
	Not e1 -> translateExpr1 NotM e1
	Isnum e1 -> translateExpr1 IsnumM e1
	And e1 e2 -> translateExpr2 AndM e1 e2
	Or e1 e2 -> translateExpr2 OrM e1 e2
	Head e1 -> translateExpr1 HeadM e1
	Tail e1 -> translateExpr1 TailM e1
	
	Call s -> CallM s

translateExpr2 :: (Term -> Term -> Term) -> Expr -> Expr -> Term
translateExpr2 c e1 e2 = c (translateExpr e1) (translateExpr e2)

translateExpr1 :: (Term -> Term) -> Expr -> Term
translateExpr1 c e1 = let t1 = translateExpr e1 in c t1

-- COMPILATION =================================================================

compile :: Prog -> MaybeUnbox -> String
compile p u = let
	(MProg fs) = translate p
	(s0, c) = compileFuncs fs (setUnbox u initContext)
	w0 = intercalate "\n" (whiles c)
	in (frontStr "\n" s0) ++ (frontStr "\n" w0)

compileFuncs :: [FuncM] -> Context -> (String, Context)
compileFuncs [] c = ([], c)
compileFuncs (f:fs) c0 = let
	(s1, c1) = compileFunc f c0
	(s2, c2) = compileFuncs fs (setWhiles (whiles c1) initContext)
	in (s1 ++ (frontStr "\n" s2), c2)

compileFunc :: FuncM -> Context -> (String, Context)
compileFunc (FuncM name rule) c0 = let
	(r, c1) = compileRule rule c0
	f = ":- func " ++ funcCallSig name (repeat2 4 lumName) ++ " = " ++ lumName
		++ ".\n" ++ funcCallSig name (enumAllVars initVarsCounts)
		++ " = " ++ r ++ "\n"
	in (f, c1)

compileRule :: Rule -> Context -> (String, Context)
compileRule (Rule t g) c0 = let
	(s1, c1) = compileGoal g c0
	vc1 = (varsCounts c1)
	(s2, c2) = compileTerm t c1
	in (s2 ++ " :- " ++ s1 ++ ".", c2)

compileGoal :: Goal -> Context -> (String, Context)
compileGoal g c0@(Context _ vc0 _ i0 ub) = case g of
	Unif v t0 -> let
		(s1, c1) = compileTerm t0 c0
		vc1 = incrVarCount v vc0
		-- For use in WhileM: Output Var (as here) is also an input Var,
		-- but input Var is not necessarily output Var
		uv1 = usedVars c1
		iv1 = inVars uv1
		ov1 = outVars uv1
		uv2 = UsedVars (setBoolVar v True iv1) (setBoolVar v True ov1)
		c2 = setVarsCounts vc1 c1
		c3 = setUsedVars uv2 c2
		-- Term return type is int
		c4 = if intMode (intVars c3) then let
			it1 = intVars c3
			ic1 = intCurr it1
			ic2 = (setBoolVar v True ic1)
			it2 = setIntCurr ic2 it1
			in setIntVars it2 c3
		else c3
		in (enumVar v vc1 ++ " = " ++ s1, c4)
	Conj gs -> let (ss, c1) = (compileGoals gs c0) in (comma ss, c1)
	Disj gs -> let (ss, c1) = (compileGoals gs c0) in (intercalate ";" ss, c1)
	ITEM t0 gt0 ge0 -> let
		(si, c1, swap) = optimalBranch t0 c0
		(gt1, ge1) = if swap then (ge0, gt0) else (gt0, ge0)
		
		(st, ct) = compileGoal gt1 c1
		(se, ce) = compileGoal ge1 (setVarsCounts vc0 ct)
		-- Reconcile missing Unif in then and else
		(vc1, st2, se2) = reconcileUnif (varsCounts ct) (varsCounts ce)
		-- Updated VarsCounts
		c2 = setVarsCounts vc1 ce

		st3 = comma st2
		st4 = if st2 /= [] then (if st /= [] then "," ++ st3 else st3) else ""
		se3 = comma se2
		se4 = if se2 /= [] then (if se /= [] then "," ++ se3 else se3) else ""

		in ("(" ++ si ++ " -> " ++ st ++ st4 ++ " ; " ++ se ++ se4 ++ ")", c2)
		
	WhileM (Rule t wg) -> let
		-- Reset all used variable bools to track which are needed in WhileM
		cu = setUsedVars initUsedVars c0
		
		-- Whether we should unbox or not
		m1 = case ub of
			Unbox -> True
			NoUnbox -> False
		c1 = setIntVars (setIntMode m1 i0) cu

		(sg1, c2) = compileGoal wg c1
		vc1 = varsCounts c2
		-- Input and output Vars
		u1 = usedVars c2
		iv1 = inVars u1
		ov1 = outVars u1
		w1 = whiles c2

		-- Base case of the While loop, returning output Vars
		bc1 = comma (map (\v -> (show v) ++ "0 = " ++ (show v)
			++ show (varCount v vc0)) (boolVarsToList ov1))

		-- While branch comes before the Goal, should use original VarsCounts
		(si, cb, swap) = optimalBranch t (setVarsCounts vc0 c2)
		c3 = (setVarsCounts vc1 cb)

		-- Only increment Vars used in output of While for use in "out"
		-- arguments of While predicate
		vc2 = incrVarCounts ov1 vc1

		-- Partial application
		argsIn = enumVars iv1
		argsOut = enumVars ov1

		-- Found at end of While block for recursion
		rcall = funcSig name (argsIn vc1 ++ argsOut nilVarsCounts)
		sg2 = sg1 ++ "," ++ rcall
		-- Swap branches if needed
		(st, se) = if swap then (bc1, sg2) else (sg2, bc1)

		-- Other strings
		name = whilePrefix ++ show (length w1)
		header = funcSig name (argsIn vc0 ++ argsOut nilVarsCounts) ++ " :-\n"
		-- Found in the caller function
		call = funcSig name (argsIn vc0 ++ argsOut vc2)

		-- Predicate definition arguments
		pargs = whileArgs (usedVars c3) (intVars c3)
		-- Declaration and definition
		d = ":- pred " ++ funcSig name pargs ++" is det.\n"
		 	++ header ++ "(" ++ si ++ " -> " ++ st ++ " ; " ++ se ++ ").\n"

		-- We don't retain any info about which intVars were used in the while
		-- or the intMode, since we don't want them to propogate anywhere except
		-- inside a WhileM
		c4 = Context (w1 ++ [d]) vc2 u1 i0 ub

		in (call, c4)

whileArgs :: UsedVars -> IntVars -> [String]
whileArgs (UsedVars ui0 uo0) (IntVars _ ii0 ic0) = let
	ui1 = boolVarsToBoolList ui0
	ii1 = boolVarsToBoolList ii0
	uo1 = boolVarsToBoolList uo0
	ic1 = boolVarsToBoolList ic0
	s1 = whileArgs' "in" ui1 ii1 []
	s2 = whileArgs' "out" uo1 ic1 []
	in s1 ++ s2

whileArgs' :: String -> [Bool] -> [Bool] -> [String] -> [String]
whileArgs' _ [] _ a = a
whileArgs' _ _ [] a = a
whileArgs' s (x:xs) (y:ys) a =
	if x then
		let t = if y then "int" else lumName in
		whileArgs' s xs ys (a ++ [t ++ "::" ++ s])
	else
		whileArgs' s xs ys a

compileGoals :: [Goal] -> Context -> ([String], Context)
compileGoals [] c = ([], c)
compileGoals (g:gs) c0 = let
	(s, c1) = compileGoal g c0
	(ss, c2) = compileGoals gs c1
	in ((s:ss), c2)

compileTerm :: Term -> Context -> (String, Context)
compileTerm t c = let
	-- intTerm is False originally
	(s1, c1, intTerm) = compileTerm' t c False
	iv1 = intVars c1
	-- Return is type int if intMode && intTerm
	ir = intTerm && intMode iv1
	c2 = setIntVars (setIntMode ir iv1) c1
	in (s1, c2)

compileTerm' :: Term -> Context -> Bool -> (String, Context, Bool)
compileTerm' t c0@(Context _ vc uv0@(UsedVars iv0 _) (IntVars m0 ii0 ic0) _) it = case t of
	VarM v -> let 
		-- Mark as input Var
		uv1 = setInVars (setBoolVar v True iv0) uv0
		s1 = enumVar v vc

		(s2, ii1, ic1) =
			if m0 then
				if it then
					if not (getBoolVar v iv0) then
						-- Add as intIn and intCurr
						(s1, (setBoolVar v True ii0), (setBoolVar v True ic0))
					else (s1, ii0, ic0)
				else
					if (getBoolVar v ic0) then
						-- Integer used in non-int Term - boxing again
						(funcRuntimeSig "num" [s1], ii0, ic0)
					else (s1, ii0, ic0)
			else (s1, ii0, ic0)
			
		c1 = setUsedVars uv1 c0
		c2 = setIntVars (IntVars m0 ii1 ic1) c1

		in (s2, c2, it)
	-- TODO num can also become int
	NumM n -> (funcRuntimeSig "num" [show n], c0, it)
	NilM -> (runTimePrefix ++ "nil", c0, it)
	ConsM t1 t2 -> compile2Term "cons" t1 t2 c0 it

	-- Operators
	PlusM t1 t2 -> compile2Term "plus" t1 t2 c0 True
	MinusM t1 t2 -> compile2Term "minus" t1 t2 c0 True
	TimesM t1 t2 -> compile2Term "times" t1 t2 c0 True
	DivM t1 t2 -> compile2Term "div" t1 t2 c0 True

	-- Boolean
	EqualM t1 t2 -> compile2Term "equal" t1 t2 c0 it
	LessM t1 t2 -> compile2Term "less" t1 t2 c0 it
	GreaterM t1 t2 -> compile2Term "greater" t1 t2 c0 it
	NotM t1 -> compile1Term "not" t1 c0 it
	IsnumM t1 -> compile1Term "isnum" t1 c0 it
	AndM t1 t2 -> compile2Term "and" t1 t2 c0 it
	OrM t1 t2 -> compile2Term "or" t1 t2 c0 it

	-- Lumber Operators
	HeadM t1 -> compile1Term "head" t1 c0 it
	TailM t1 -> compile1Term "tail" t1 c0 it

	-- Function calls
	CallM funcName -> let
		-- Mark all Vars as inputs
		uv1 = setInVars trueBoolVars uv0
		in (funcCallSig funcName (enumAllVars vc), setUsedVars uv1 c0, it)
	
-- Compiles one expression given a runtime function name
compile1Term :: String -> Term -> Context -> Bool -> (String, Context, Bool)
compile1Term n0 t c0 it0 = let
	(s1, c1, it1) = compileTerm' t c0 it0
	n1 = if intMode (intVars c0) && it1 then n0 ++ "i" else n0
	in (funcRuntimeSig n1 [s1], c1, it1)

-- Compiles two expressions given a runtime function name
compile2Term :: String -> Term -> Term -> Context -> Bool -> (String, Context, Bool)
compile2Term n0 t1 t2 c0 it0 = let
	(s1, c1, it1) = compileTerm' t1 c0 it0
	(s2, c2, it2) = compileTerm' t2 c1 it1
	n1 = if intMode (intVars c2) && it2 then n0 ++ "i" else n0
	in (funcRuntimeSig n1 [s1, s2], c2, it2)

-- AUXILIARY GOAL FUNCTIONS ====================================================

reconcileUnif :: VarsCounts -> VarsCounts -> (VarsCounts, [String], [String])
reconcileUnif (VarsCounts a1 b1 c1 d1) (VarsCounts a2 b2 c2 d2) = let
	(xs1, ys1) = reconcileUnif' D d1 d2 [] []
	(xs2, ys2) = reconcileUnif' C c1 c2 xs1 ys1
	(xs3, ys3) = reconcileUnif' B b1 b2 xs2 ys2
	(xs4, ys4) = reconcileUnif' A a1 a2 xs3 ys3
	c = VarsCounts (max a1 a2) (max b1 b2) (max c1 c2) (max d1 d2)
	in (c, xs4, ys4)

reconcileUnif' ::
	Var -> Int -> Int -> [String] -> [String] ->([String], [String])
reconcileUnif' v x y xs ys =
	if x > y then
		(xs, (enumVarInt v x ++ " = " ++ enumVarInt v y):ys)
	else if y > x then
		((enumVarInt v y ++ " = " ++ enumVarInt v x):xs, ys)
	else
		(xs, ys)

-- We can exploit the fact that a condition in Mercury is a Boolean and:
-- 1. Transform negative conditions into positive and swap branches
-- 2. Avoid returning a lumber from the comparison since we only need a Boolean
optimalBranch :: Term -> Context -> (String, Context, Bool)
optimalBranch t0 c0 = let
    -- If NotM is used in condition, use the Term within it and indicate
	-- branches should be swapped
    (t1, b) = case t0 of
    	NotM nt -> (nt, True)
    	_ -> (t0, False)

    -- If EqualM then transform into simple comparison
    (s3, c3) = case t1 of
    	EqualM e1 e2 -> let
    		(s1, c1) = (compileTerm e1 c0)
    		(s2, c2) = (compileTerm e2 c1)
    		in (s1 ++ " = " ++ s2, c2)
    	_ -> let (s1, c1) = compileTerm t1 c0 in
    		(s1 ++ " = " ++ funcRuntimeSig "num" ["1"], c1)

	in (s3, c3, b)

-- AUXILIARY DATA & FUNCTIONS ==================================================

data MaybeUnbox = Unbox | NoUnbox deriving Show

data Context = Context {
	whiles :: [String],
	varsCounts :: VarsCounts,
	usedVars :: UsedVars,
	intVars :: IntVars,
	unBoxed :: MaybeUnbox
	} deriving Show

data UsedVars = UsedVars {inVars :: BoolVars, outVars :: BoolVars} deriving Show
data IntVars = IntVars {
	intMode :: Bool,
	intIn :: BoolVars,
	intCurr :: BoolVars
	} deriving Show

initContext :: Context
initContext = Context [] initVarsCounts initUsedVars initIntVars NoUnbox

initIntVars :: IntVars
initIntVars = IntVars False falseBoolVars falseBoolVars

initUsedVars :: UsedVars
initUsedVars = UsedVars falseBoolVars falseBoolVars

setUnbox :: MaybeUnbox -> Context -> Context
setUnbox b (Context w v u i _) = Context w v u i b

setVarsCounts :: VarsCounts -> Context -> Context
setVarsCounts v (Context w _ u i b) = Context w v u i b

setUsedVars :: UsedVars -> Context -> Context
setUsedVars u (Context w v _ i b) = Context w v u i b

setInVars :: BoolVars -> UsedVars -> UsedVars
setInVars i (UsedVars _ o) = UsedVars i o

setOutVars :: BoolVars -> UsedVars -> UsedVars
setOutVars o (UsedVars i _) = UsedVars i o

setIntVars :: IntVars -> Context -> Context
setIntVars i (Context w v u _ b) = Context w v u i b

setIntMode :: Bool -> IntVars -> IntVars
setIntMode m (IntVars _ i c) = IntVars m i c

setIntIn :: BoolVars -> IntVars -> IntVars
setIntIn i (IntVars m _ c) = IntVars m i c

setIntCurr :: BoolVars -> IntVars -> IntVars
setIntCurr c (IntVars m i _) = IntVars m i c

setWhiles :: [String] -> Context -> Context
setWhiles ws (Context _ v u i b) = Context ws v u i b

addWhile :: String -> Context -> Context
addWhile w (Context ws v u i b) = Context (w:ws) v u i b

data VarsCounts = VarsCounts {
	varA :: Int,
	varB :: Int,
	varC :: Int,
	varD :: Int
	} deriving Show

incrVarCount :: Var -> VarsCounts -> VarsCounts
incrVarCount v (VarsCounts a b c d) = case v of
	A -> VarsCounts (a+1) b c d
	B -> VarsCounts a (b+1) c d
	C -> VarsCounts a b (c+1) d
	D -> VarsCounts a b c (d+1)

incrVarCounts :: BoolVars -> VarsCounts -> VarsCounts
incrVarCounts (BoolVars ba bb bc bd) (VarsCounts a b c d) = let
	ra = if ba then (a+1) else a
	rb = if bb then (b+1) else b
	rc = if bc then (c+1) else c
	rd = if bd then (d+1) else d
	in VarsCounts ra rb rc rd

incrAllVarCounts :: VarsCounts -> VarsCounts
incrAllVarCounts (VarsCounts a b c d) = VarsCounts (a+1) (b+1) (c+1) (d+1)

initVarsCounts :: VarsCounts
initVarsCounts = VarsCounts 1 1 1 1

nilVarsCounts :: VarsCounts
nilVarsCounts = VarsCounts 0 0 0 0

funcRuntimeSig :: String -> [String] -> String
funcRuntimeSig name args = runTimePrefix ++ funcSig name args

funcCallSig :: String -> [String] -> String
funcCallSig name args = funcNamePrefix ++ funcSig name args

varCount :: Var -> VarsCounts -> Int
varCount v vc = case v of
	A -> varA vc
	B -> varB vc
	C -> varC vc
	D -> varD vc

enumAllVars :: VarsCounts -> [String]
enumAllVars vc = enumVars trueBoolVars vc

enumVars :: BoolVars -> VarsCounts -> [String]
enumVars (BoolVars a b c d) vc = let
	sd = enumVar' D d vc []
	sc = enumVar' C c vc sd
	sb = enumVar' B b vc sc
	sa = enumVar' A a vc sb
	in sa

enumVar' :: Var -> Bool -> VarsCounts -> [String] -> [String]
enumVar' v b vc a = if b then (enumVar v vc):a else a

boolVarsToList :: BoolVars -> [Var]
boolVarsToList (BoolVars a b c d) = filter2 [(a, A), (b, B), (c, C), (d, D)]

boolVarsToBoolList :: BoolVars -> [Bool]
boolVarsToBoolList (BoolVars a b c d) = [a,b,c,d]

-- Enumerates a Var based on a VarsCounts as a String
enumVar :: Var -> VarsCounts -> String
enumVar v vc = show v ++ show (varCount v vc)

enumVarInt :: Var -> Int -> String
enumVarInt v vc = show v ++ show vc
