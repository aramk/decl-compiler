-------------------------------------------------------------------------------
--
-- vim: sw=4 ts=4 et ft=haskell
--
-- A set of generic functions
--
-- HIDE_START
-- Written by Aram Kocharyan, aramk
-- HIDE_END
--

module Util(comma, repeat2, filter2, strbasename, frontStr, funcSig) where

import Data.List

-- Repeats something a given number of times as a list
repeat2 :: Int -> b -> [b]
repeat2 n r = take n (repeat r)

-- Joins a list of Strings with commas
comma :: [String] -> String
comma xs = intercalate "," xs

filter2 :: [(Bool, a)] -> [a]
filter2 [] = []
filter2 ((b, x):bs) =
	if b then x:r
	else r
	where r = (filter2 bs)

-- Given "/some/path/filename", returns "filename"
strbasename :: String -> String
strbasename s = let i = findIndex (\x -> x == '/') s in case i of
	Just index -> strbasename (drop (index + 1) s)
	Nothing -> s

-- Adds String to front of String if length > 0
frontStr :: String -> String -> String
frontStr a b = if length b > 0 then a ++ b else b

-- Turns a function generic signature into a String
funcSig :: String -> [String] -> String
funcSig name args = name ++ "(" ++ comma args  ++ ")"
