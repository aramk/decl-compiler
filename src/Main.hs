-------------------------------------------------------------------------------
--
-- vim: sw=4 ts=4 et ft=haskell
--
-- The top level of compiler for the ABCD language.
--
-- Written by Lee Naish
-- (based in interpreter top level written by Zoltan Somogyi).
--

module Main(main) where

import Data.List
import System.Environment

import Util
import Prog
import MProg
import Parse
import Compiler

main :: IO ()
main = do
	progname <- getProgName
	args <- getArgs
	process_args progname args "main" NoPrintProg NoUnbox

process_args :: String -> [String] -> String -> MaybePrintProg -> MaybeUnbox -> IO ()
process_args progname [] _ _ _ = do
	usage progname
process_args progname (arg:args) entry print_prog unbox = let s = (drop 2 arg) in
	if isPrefixOf "-e" arg then
		process_args progname args s print_prog unbox
	else if arg == "-p" then
        process_args progname args entry PrintProg unbox
	else if arg == "-u" then
        process_args progname args entry print_prog Unbox
	else if args == [] then
		process_prog progname arg entry print_prog unbox
	else
		usage progname

process_prog :: String -> String -> String -> MaybePrintProg -> MaybeUnbox -> IO()
process_prog _progname filename entry print_prog unbox = let
	filename2 =
		if isSuffixOf ".abcd" filename then rm_extension filename
		else filename
	in do
		maybeprog <- parse_prog_file (filename2 ++ ".abcd")
		maybe_compile_prog maybeprog (strbasename filename2) entry print_prog unbox

-- remove .abcd
rm_extension :: String -> String
rm_extension name =
	take ((length name) - 5) name

read_var :: Int -> IO Val
read_var line_number = do
	line_chars <- getLine
	let maybe_lumber = parse_lumber line_number line_chars
	case maybe_lumber of
		Error msg ->
			do
				putStrLn ("Error on variable line " ++ (show line_number) ++
					": " ++ msg ++ ".")
				putStrLn "Substituting zero."
				return (Num 0)
		OK lumber ->
			return lumber

usage :: String -> IO ()
usage progname =
	putStrLn ("Usage: " ++ progname ++ " [-efunc] [-p] [-u] Name\n" ++
		"Where Name.abcd contains an ABCD program, func is an entry function.\nSupports standard input via redirection.")

maybe_compile_prog :: MaybeOK Prog -> String -> String -> MaybePrintProg -> MaybeUnbox -> IO ()
maybe_compile_prog (Error msg) _ _ _ _ = do
	putStrLn ("error: " ++ msg)
maybe_compile_prog (OK prog) modulename entry maybe_print unbox =
	case verify_entry prog entry of
		Error s -> putStrLn $ "abcdc: " ++ s
		OK _ -> do
			h <- header modulename entry
			let
				compiled = h ++ compile prog unbox
				print_prog = case maybe_print of
					PrintProg -> compiled
					NoPrintProg -> ""
			putStr print_prog
			writeFile (modulename ++ ".m") compiled

-- Find all Func matching the entry name, ensure only 1 exists
-- Also reports potential entry functions if the given entry isn't found
verify_entry :: Prog -> String -> MaybeOK ()
verify_entry (Prog funcs) entry = let
	m = filter (\f -> func_name f == entry) funcs
	l = length m in
	if l == 0 then
		Error $ "No entry '" ++ entry ++ "' found!\nTry: "
			++ (comma $ map (\f -> func_name f) funcs)
	else if l > 1 then
		Error $ "More than 1 entry '" ++ entry ++ "' found!"
	else
		OK ()

header :: String -> String -> IO (String)
header modulename entry = do
	a <- read_var 1
	b <- read_var 2
	c <- read_var 3
	d <- read_var 4
	return $
		"% Compiled ABCD program\n:- module " ++ modulename ++
		". :- interface.\n:- pred main(io.state::di, io.state::uo) is det.\n" ++
		":- import_module io.\n:- implementation.\n:- import_module abcdrt.\n" ++
		"main --> abcdrt_write_lum(" ++ funcNamePrefix ++ entry ++
		"(" ++ valToRuntime a ++ "," ++ valToRuntime b ++ "," ++ valToRuntime c ++
		"," ++ valToRuntime d ++ ")), nl.\n"

valToRuntime :: Val -> String
valToRuntime (Num n) = funcRuntimeSig "num" [show n]
valToRuntime Nil = runTimePrefix ++ "nil"
valToRuntime (Cons v1 v2) = funcRuntimeSig "cons" [valToRuntime v1, valToRuntime v2]
